public class MyDate {
    private int day;
    private int month;
    private int year;

    int[] maxDays = {31,29,31,30,31,30,31,31,30,31,30,31};

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month-1;
        this.year = year;
    }

    public void decrementDay(){
        int newDay = day-1;
        if(newDay == 0){
            day = 31;
            decrementMonth();
        }else{
            day = newDay;
        }
    }

    public void decrementDay(int diff){
        while (diff > 0){
            decrementDay();
            diff--;
        }

    }

    public void decrementYear(){
        incrementYear(-1);
    }

    public void decrementYear(int year) {
        incrementYear(-year);
    }

    public void decrementMonth(){
        incrementMonth(-1);
    }

    public void decrementMonth(int month){
        incrementMonth(-month);
    }

    public void incrementDay(int diff){
        while (diff > 0){
            incrementDay();
            diff--;
        }
    }

    public void incrementDay(){
        int newDay = day + 1;
        int maxDay = maxDays[month];
        if(newDay > maxDay){
            incrementMonth();
            day = 1;
        }else if (month == 1 && newDay == 29 && !leapYear()){
            day = 1;
            incrementMonth();
        }else{
            day = newDay;
        }
    }


    public void incrementYear(int diff){
        year += diff;
        if(month == 1 && day == 29 && !leapYear()){
            day = 28;
        }
    }

    public void incrementYear(){
        incrementYear(1);
    }

    public void incrementMonth(int diff){
        int newMonth = (month + diff)%12;
        int yearDiff = 0;

        if(newMonth < 0){
            newMonth += 12;
            yearDiff = -1;
        }
        yearDiff += (month + diff)/12;
        month = newMonth;
        year += yearDiff;

        if(day > maxDays[month]){
            day = maxDays[month];
            if (month == 1 && day == 29 && !leapYear()){
                day = 28;
            }
        }

    }
    public void incrementMonth(){
        incrementMonth(1);
    }

    public boolean leapYear(){
        return year % 4 == 0 ? true : false;
    }

    public boolean isBefore(MyDate anotherDate){
        boolean years = year < anotherDate.year ? true:false;
        boolean months = month < anotherDate.month ? true:false;
        boolean days = day < anotherDate.day ? true:false;

        if(years){
            return true;
        }else if(year == anotherDate.year){
            if(months){
                return true;
            }else if(month == anotherDate.month){
                if(days){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isAfter(MyDate anotherDate){
        boolean years = year < anotherDate.year ? false:true;
        boolean months = month < anotherDate.month ? false:true;
        boolean days = day < anotherDate.day ? false:true;

        if(years){
            return true;
        }else if(year == anotherDate.year){
            if(months){
                return true;
            }else if(month == anotherDate.month){
                if(days){
                    return true;
                }
            }
        }
        return false;
    }

    public int dayDifference(MyDate anotherDate) {
        int result = 0;

        if (year > anotherDate.year) {
            if (month > anotherDate.month) {
                if (day > anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (day - anotherDate.day);
                }else if ((month - anotherDate.month == 1) && day > anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + (maxDays[anotherDate.month] - anotherDate.day) + day;
                }else if (day < anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (maxDays[anotherDate.month] - anotherDate.day) + day;
                } else if ((month - anotherDate.month == 1) && day < anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + (maxDays[month] - day) + anotherDate.day;
                }
            } else if ((year - anotherDate.year == 1) && month < anotherDate.month) {
                if (day > anotherDate.day) {
                    result = result + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (day - anotherDate.day);
                } else if (day < anotherDate.day) {
                    result = result + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (maxDays[anotherDate.month] - anotherDate.day) + day;
                }
            } else if (month < anotherDate.month) {
                if (day > anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (day - anotherDate.day);
                } else if (day < anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (maxDays[anotherDate.month] - anotherDate.day) + day;
                }
            }
            return result;
        } else if (year < anotherDate.year) {
            if (month > anotherDate.month) {
                if (day > anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (day - anotherDate.day);
                }else if ((month - anotherDate.month == 1) && day > anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + (maxDays[anotherDate.month] - anotherDate.day) + day;
                }else if (day < anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (maxDays[anotherDate.month] - anotherDate.day) + day;
                } else if ((month - anotherDate.month == 1) && day < anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + (maxDays[month] - day) + anotherDate.day;
                }
            } else if ((year - anotherDate.year == 1) && month < anotherDate.month) {
                if (day > anotherDate.day) {
                    result = result + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (day - anotherDate.day);
                } else if (day < anotherDate.day) {
                    result = result + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (maxDays[anotherDate.month] - anotherDate.day) + day;
                }
            } else if (month < anotherDate.month) {
                if (day > anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (day - anotherDate.day);
                } else if (day < anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (maxDays[anotherDate.month] - anotherDate.day) + day;
                }
            }
            return result;
        }else if(year == anotherDate.year){
            if (month > anotherDate.month) {
                if (day > anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (day - anotherDate.day);
                }else if ((month - anotherDate.month == 1) && day > anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + (maxDays[anotherDate.month] - anotherDate.day) + day -1;
                }else if (day < anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (maxDays[anotherDate.month] - anotherDate.day) + day;
                } else if ((month - anotherDate.month == 1) && day < anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + (maxDays[month] - day) + anotherDate.day;
                }
            } else if ((year - anotherDate.year == 1) && month < anotherDate.month) {
                if (day > anotherDate.day) {
                    result = result + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (day - anotherDate.day);
                } else if (day < anotherDate.day) {
                    result = result + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (maxDays[anotherDate.month] - anotherDate.day) + day;
                }
            } else if (month < anotherDate.month) {
                if (day > anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (day - anotherDate.day);
                } else if (day < anotherDate.day) {
                    result = result + ((year - anotherDate.year) * 365)
                            + ((month - anotherDate.month) * maxDays[anotherDate.month])
                            + (maxDays[anotherDate.month] - anotherDate.day) + day;
                }
            }
        }
        return result;
    }

    public String toString(){
        return year + "-" + ((month+1) < 10 ? "0" : "") + (month+1) + "-" + (day < 10 ? "0" : "") + day;
    }

    public static void main(String[] args){
    }
}