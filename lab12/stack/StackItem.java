package stack;

public class StackItem {
    private Object item;
    private StackItem next;

    public StackItem getNext() {
        return next;
    }

    public Object getItem() {
        return item;
    }

    public void setNext(StackItem next) {
        this.next = next;
    }

    public StackItem(Object item){
        this.item = item;


    }
}
