package drawing.version3;

import java.sql.SQLOutput;
import java.util.ArrayList;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Shape;
import shapes.Square;

public class Drawing {

	private ArrayList<Shape> shapes = new ArrayList<Shape>();

	public double calculateTotalArea(){
		double totalArea = 0;

		for (Shape shape : shapes){ // you can't modife object class , bcs it's java class
			System.out.println(shape.getClass());
			totalArea += shape.area();
		}
		return totalArea;
	}

	public void addShape(Shape shape) {
		shapes.add(shape);
	}
}
