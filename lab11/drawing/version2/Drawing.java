package drawing.version2;

import java.util.ArrayList;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;

public class Drawing {
	
	private ArrayList<Object> shapes = new ArrayList<Object>(); //much more general list.
	// every class is subclass of object.
	
	public double calculateTotalArea(){
		double totalArea = 0;

		for (Object shape : shapes){ // you can't modife object class , bcs it's java class
			System.out.println(shape.getClass());
			if(shape instanceof  Circle) {
				Circle c = (Circle) shape; //casting
				totalArea += c.area();
			}else if (shape instanceof Rectangle){
				Rectangle r = (Rectangle) shape; //casting
				totalArea += r.area();
			}else if (shape instanceof Square){
				Square s = (Square) shape; //casting
				totalArea += s.area();
			}
		}
		return totalArea;
	}
	
	public void addShape(Object shape) {
		shapes.add(shape);
	}
}
